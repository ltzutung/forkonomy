# forkonomy() 

A co-writing-forking-performing project by Soon Winnie and Lee Tzu Tung

## setup()
    
## start()
   
## Exercise

traceroute()

generateQuestions()


## Source (code)
[RUNME](https://editor.p5js.org/siusoon/present/k6xUx6nbc)

```javascript
let questions = 
    ["What it means by, or how could we imagine, forking economy as an ongoing gene space? ", 
     " How could we fork the forktopia?",
     "Would forking economy help to save those pressing issues, such as having constant threats in maintaining autonomy, in both Taiwan and Hong Kong?",
     "Does econonmy exists before 18th century Taiwan, or just piracy?",
     "Why has power in the West assumed the form of an “econonmy”, of a government encompasses all men, productive activities and things?",
     "What’s left without the conclusion?",
     "What’s the possibility of a forked historical text?"
    ];

let val = 0;

function draw() {
  background(28,36,40);
  forkonomy();      
}

function forkonomy() {
  let question = random(questions);
  fill (220,226,229);
  textSize(width*0.03);
  textLeading(width*0.04);
  text('a queer ocean of freedom and the sea of commonwealth:', width/20, height/2.5);
  text(question, width/20, height/2.5 + width*0.04, width/1.1); 
}

function mousePressed() {
  val += 1;
  if (val % 2 != 0) {
    noLoop();
  }else{
	loop();
  };
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

```
## While()
    
    
## Reference
- Kingdom of Piracy: http://yukikoshikata.com/kingdom-of-piracy/#en
- Submarine cable map: https://www.submarinecablemap.com/
- traceroute-online: https://traceroute-online.com/
- creating commons: http://creatingcommons.zhdk.ch/
- 1st prototype by Artemis: https://pzwiki.wdka.nl/mediadesign/User:Artemis_gryllaki/Prototyping

## Notes: 
dialogues with audience


--- 
# Description

In contemporary economical expansion, America federal and banks, giant tech corporations have managed to maintain their colonial forces to govern the accessibility and distribution of resources. The rise of China economic power, though operating in a different regime, also follows and adopts similar capitalistic, centralized and advanced (computable) machines, in which the invisible hand exploits ecologies and alienates labors. The economy trade bloc further demarcates the nowaday island states and territories of the Pacific.

However, many people have lived and traversed through the sea, between islands, coastlines, and cities for over two thousand years. Taiwan as the northeast Austronesians and Hong Kong 蜑家 Dang-Chia as “ocean peoples” viewed the world as “a sea of islands” rather than “places along the continent”. With the history and development of port infrastructure, it is not surprised that bartering and piracy on material and intangible wealth were the precursors to the monetary system.

Through Forkonomy(), we are interested in rethinking the politics of our contemporary economic and technical-cultural systems. By employing free and open source software and decentralized protocols, we set the participatory project as a commoning ship for people of the pacific who want to queer the matters of hierarchies, ownership, gendered labor division, as well as to fight against the constant threats of maintaining a high degree of autonomy regarding the land and the sea. Forknomy() shall sail the economy and autonomy into a queer ocean of freedom and the sea of commonwealth. 
